import os
import json
import subprocess


class generateNewPackage:
    def __init__(self):
        self.packageName = None
        self.softwareName = None
        self.description = None
        self.version = None
        self.enable64bit = False
        self.downloadUrl = None
        self.downloadUrl64 = None
        self.checksum = None
        self.checksum64 = None
        self.checksumType = None
        self.checksumType64 = None
        self.fileType = None
        self.silentArgs = None
        self.validExitCodes = []
        self.uninstallSilenArgs = None
        self.icon = None
        self.unzip = None
        self.dict = {}

    def name(self):
        package = input("Package Name for application. Example: google-chrome*: ")
        self.packageName = package

    def sname(self):
        package = input("Software Name for application. Example: Google Chrome*: ")
        self.softwareName = package

    def desc(self):
        package = input("Description for application. This will be published on website*: ")
        self.description = package

    def ver(self):
        package = input("Version number for application. This will be published on website*: ")
        self.version = package

    def is64bit(self):
        package = input("Does this application has 64 bit product? [Y/N]*: ")

        if package == "y":
            self.enable64bit = True
        else:
            self.enable64bit = False

    def durl(self):
        if self.enable64bit:
            package64 = input("Download url for 64-bit product: ")
            self.downloadUrl64 = package64
        package = input("Download url for 32-bit product: ")
        self.downloadUrl = package

    def ctype(self):
        if self.enable64bit:
            checksumType64 = input("Checksum type for 64-bit application [sha256, md5]: ")
            self.checksumType64 = checksumType64

        checksumType = input("Checksum type for 32-bit application [sha256, md5]: ")
        self.checksumType = checksumType

    def checks(self):
        if self.enable64bit:
            checksum64 = input("Checksum for 64-bit application: ")
            self.checksum64 = checksum64

        checksumType = input("Checksum for 32-bit application: ")
        self.checksum = checksumType

    def filet(self):
        package = input("Filetype for application. Ex: [exe, msi, 7z, zip]: ")
        self.fileType = package

    def silenta(self):
        package = input("Silent args for application. Ex: [/S]: ")
        self.silentArgs = package

    def exitCodes(self):
        package = input(
            "Exit codes for application: (We detect if program exited successfully within this codes.) Example: 0,1,2: ")
        self.validExitCodes = package.split(",")

    def usilenta(self):
        package = input("Silent args for uninstallation of application. Ex: [/S]: ")
        self.uninstallSilenArgs = package

    def packageIcon(self):
        package = input("Icon URL for application: ")
        self.icon = package

    def isunzip(self):
        package = input("Is this application compressed with zip technologies? [Y/N]: ")
        if package == "y":
            self.unzip = True


class generatePackage(generateNewPackage):

    def getAnswers(self):
        self.name()
        self.sname()
        self.desc()
        self.ver()
        self.isunzip()
        self.is64bit()
        self.durl()
        self.ctype()
        self.checks()
        self.filet()
        self.silenta()
        self.exitCodes()
        self.usilenta()
        self.packageIcon()

    def generateJson(self):

        dict = {
            "packageArgs": {
                "packageName": self.packageName,
                "softwareName": self.softwareName,
                "description": self.description,
                "version": self.version,
                "downloadUrl": self.downloadUrl,
                "checksum": self.checksum,
                "checksumType": self.checksumType,
                "fileType": self.fileType,
                "silentArgs": self.silentArgs,
                "validExitCodes": self.validExitCodes
            },

            "packageUninstallArgs": {
                "silentArgs": self.uninstallSilenArgs
            },

            "server": {
                "icon": "https://gitlab.com/muhammedkpln/coban-packages/raw/master/icons/{0}.png".format(self.icon)
            }
        }

        if self.unzip != None and self.unzip == True:
            unzip = {
                "unzip": True
            }

            dict["packageArgs"].update(unzip)

        if self.enable64bit:
            dict64 = {
                "downloadUrl64": self.downloadUrl64,
                "checksum64": self.checksum64,
                "checksumType64": self.checksumType64
            }

            dict["packageArgs"].update(dict64)

            self.dict = dict

        self.dict = dict

        return self.dict

    def writeToFile(self, dict={}):
        path = input(
            "Where you want me to save disk script to?. Default (installations folder in same dir with script.): ")

        if self.dict:
            dict = self.dict
        else:
            dict = dict

        if path == "":
            path = "installations/{0}.cb".format(self.packageName)
            with open(path, "w") as f:
                f.write(json.dumps(dict, indent=4))
                f.close()


class updateProgramList:

    def __init__(self):
        packages = os.listdir("installations")
        programList = "programList.json"
        dict = {}
        packagesDoesNotExists = []
        with open(programList, "r") as f:
            js = json.loads(f.read())

            for i in packages:
                withoutExt = i.split(".")[0]

                if withoutExt in js:
                    pass
                else:
                    packagesDoesNotExists.append(withoutExt)
                    with open("programList.json", "w") as f:
                        newDict = {
                            withoutExt: "https://gitlab.com/muhammedkpln/coban-packages/raw/master/installations/{0}".format(
                                i)
                        }
                        dict = {**newDict, **js}
                        js.update(newDict)
                        f.write(json.dumps(dict, indent=4, sort_keys=True))
                        f.close()

        gitAsk = input("Do you want push to Git?: ")
        bind = ", ".join(packagesDoesNotExists)

        if gitAsk == "y":
            gitdir = os.path.dirname(os.path.realpath(__file__))
            subprocess.call("git add * .", cwd=gitdir)
            subprocess.call('git commit -m "added {0}"'.format(bind), cwd=gitdir)
            subprocess.call("git push", cwd=gitdir)


try:
    cls = generatePackage()
    cls.getAnswers()
    dict = cls.generateJson()
    cls.writeToFile(dict)

    print(generateNewPackage().packageName)
except Exception as e:
    print(e)
    pass

updateProgramList()
